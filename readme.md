
## _Lloyds Design_

# Javascript

## mobilne aplikacije 

Dobro poznavanje Javascript-a je ključno za razvoj mobilnih aplikacija.
Koristimo React Native - Javascript framework za razvoj aplikacija.
React Native je baziran na ReactJS - tehnologiji za razvoj web aplikacija.
Kada se piše Javascript (JS) kod za ReactJS i React Native mogu se koristiti moderni feature-i jezika - ES6+. 

To može biti problematično početnicima koji tek kreću učiti jer se JS dosta mjenjao kroz vrijeme i stalno je dobivao neke nove feature, pa tako sada googlanje rješenja nekog problema može dati dva, tri rješenja koja su točna, ali su iz različitih faza postojanja JS-a.

Znači, ovako... Mi u Lloyds-u trebamo mobile app developera ili web developera koji zna:
  - Moderni Javascript (ES6+) - Za učenje ne znam točno što da preporučim, ali ima dobrih tutoriala na https://www.codecademy.com/, a googlanje "modern javascript tutorial" bi također trebalo izbaciti nešto korisno
  - ReactJS - ovo je potrebno jer je React Native baziran na ovome - Official stranica ima jako dobar tutorial - https://reactjs.org/  
  - React Native - ako znas ReactJS onda ti svladavanje ovoga nece biti problem - također mogu preporučiti official stranicu za intro - https://facebook.github.io/react-native/

## web development

Web i mobile su jako slično po znanju koje je potrebno. Mi u Lloyds-u koristimo VueJS framework pa ako te zanima to pogledaj si malo na https://vuejs.org/


Za programiranje mi u Lloyds-u koristimo VSCode (https://code.visualstudio.com/).
Operativni sustav koji koristiš nije bitan.
Za isprobavanje ReactJS-a i VueJS-a bez instalacije ičega na svoj sustav možeš koristiti https://codesandbox.io/ - online editor - no ovo neka služi samo kao playground.

Nadam se da je ovo dovoljno za početak, a ako nešto nije jasno onda javi pa te dalje uputimo.

